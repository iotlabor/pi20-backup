import smbus
import time
from datetime import datetime
import json
from kafka import KafkaProducer

address = 0x28
ic = smbus.SMBus(1)

producer = KafkaProducer(bootstrap_servers=['192.168.1.10:9092'], value_serializer=lambda v: json.dumps(v).encode('utf-8'))

while True:

	time.sleep(0.2)	
	pressure = ic.read_word_data(address, 0x00)
	pressure = ((pressure >> 8) & 0x0ff) | ((pressure << 8) & 0xff00) 
	psi = (pressure-1638.0)*(30.0-(-30.0))
	psi = psi/(14745.0-1638.0)
	psi = psi+(-30.0)
	bar = psi*0.0689
	producer.send('LuftdruckSensor', {'Time':str(datetime.now()), 'Pressure' : pressure, 'PSI' : psi, 'Bar' : bar})
	print("DruckMB: " + str(pressure))
	print("PSI: " + str(psi))
	print("Bar: " +  str(bar))


