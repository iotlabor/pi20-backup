# ADXL345 Python example 
#
# author:  Jonathan Williamson
# license: BSD, see LICENSE.txt included in this package
# 
# This is an example to show you how to use our ADXL345 Python library
# http://shop.pimoroni.com/products/adafruit-triple-axis-accelerometer
import os
from adxl345 import ADXL345
import plotly.plotly as py # plotly library
from plotly.graph_objs import Scatter, Layout, Figure # plotly graph objects
import time # timer functions
import readadc # helper functions to read ADC from the Raspberry Pi
username = 'kleinp'
api_key = 'veTpgvTFlUPcfENABlD0'
stream_token = 'f8x8z6vu8q'
py.sign_in(username, api_key)
trace1 = Scatter(
    x=[],
    y=[],
    stream=dict(
        token=stream_token,
        maxpoints=200
    )
)

layout = Layout(
    title='Raspberry Pi Streaming Sensor Data ADXL345'
)

fig = Figure(data=[trace1], layout=layout)

print py.plot(fig, filename='Raspberry Pi Streaming Example Values ADXL345')
sensor_pin =53 
readadc.initialize()
stream = py.Stream(stream_token)
stream.open()

adxl345 = ADXL345()
    
while True:
	axes = adxl345.getAxes(True)
	print "ADXL345 on address 0x%x:" % (adxl345.address)
	print "   x = %.3fG" % ( axes['x'] )
	print "   y = %.3fG" % ( axes['y'] )
	print "   z = %.3fG" % ( axes['z'] )
	os.system('clear')
