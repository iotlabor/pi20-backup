from adxl345 import ADXL345
from datetime import datetime
import json
from kafka import KafkaProducer
import time

adxl345 = ADXL345()    

print "ADXL345 on address 0x%x:" % (adxl345.address)
producer = KafkaProducer(bootstrap_servers=['192.168.1.10:9092'], value_serializer=lambda v: json.dumps(v).encode('utf-8'))

while True:
	axes = adxl345.getAxes(True)
	producer.send('VibrationsSensor', {'Time':str(datetime.now()), 'x':axes['x'], 'y':axes['y'], 'z':axes['z']})
	time.sleep(0.2)
        print "   x = %.3fG" % ( axes['x'] )
        print "   y = %.3fG" % ( axes['y'] )
        print "   z = %.3fG" % ( axes['z'] )


